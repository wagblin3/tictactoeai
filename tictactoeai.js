// Tic Tac Toe Game in Javascript

// Set up board as 3x3 array 
let board = [
  ['', '', ''],
  ['', '', ''],
  ['', '', '']
];

// Track whose turn it is
let playerTurn = 'X'; 

// Function to mark square on board
function markSquare(row, col) {
  board[row][col] = playerTurn;

  // Switch turns
  if(playerTurn == 'X') {
    playerTurn = 'O';
  } else {
    playerTurn = 'X';
  }
}

// Check if someone has won
function checkWin() {
  // Check rows
  for(let i = 0; i < 3; i++) {
    if(board[i][0] != '' && board[i][0] == board[i][1] && board[i][1] == board[i][2]) {
      return true;
    }
  }

  // Check columns 
  for(let j = 0; j < 3; j++) {
    if(board[0][j] != '' && board[0][j] == board[1][j] && board[1][j] == board[2][j]) {
      return true;
    }
  }

  // Check diagonals
  if(board[0][0] != '' && board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
    return true;
  }
  if(board[0][2] != '' && board[0][2] == board[1][1] && board[1][1] == board[2][0]) {
    return true;
  }

  return false;
}

// Game loop
while(true) {
  // Get user input
  let row = parseInt(prompt('Enter row: '));
  let col = parseInt(prompt('Enter column: '));

  // Mark the square
  markSquare(row, col);

  // Print updated board
  console.log(board[0].join(' | '));
  console.log(board[1].join(' | '));
  console.log(board[2].join(' | '));

  // Check for winner
  if(checkWin()) {
    console.log(`Player ${playerTurn} wins!`);
    break;
  }

  // Check for tie
  if(board[0].every(sq => sq != '') && 
     board[1].every(sq => sq != '') &&
     board[2].every(sq => sq != '')) {
    console.log("It's a tie!");
    break;
  }
}
